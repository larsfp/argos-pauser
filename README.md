# Argos-pauser

## Overview

An [Argos](https://github.com/Coda-Coda/argos) plugin to make sure you take pauses from the screen.

Argos is a Gnome 3 extension.

## Features

- It respects "Do not desturb" mode.
- If you idle a break is recorded.
- If you unlock the screen a break is recorded.
- Configurable pause length and work length.
- Reset button in menu to record a break has been taken.

![do not desturb](img/dnd.png "Do not desturb")
![normal](img/normal.png "Normal")
![detailed](img/detailed.png "Detailed")
![unlocked](img/unlocked.png "Unlocked")
![break](img/break.png "Break")

## Requirements

```bash
sudo apt install python3-gi python3-gi-cairo gir1.2-gtk-4.0
```

```bash
pip3 install idle-time
```

or

```bash
sudo apt install libgirepository1.0-dev pkg-config python3-dev gir1.2-gtk-4.0
```

```bash
pip3 install pycairo PyGObject idle-time
```

## Installation

Symlink script to your argos dir. I.e.

```bash
ln -s ~/src/argos-pauser/pauser.py ~/.config/argos/pauser.10s.py
```

## Configuration

- Config file can be opened from menu.
- String searched for in journalctl to check if screen has been unlocked is called "needle". It may need adaptation to match your setup. Find out what your system loggs when session is unlocked. I used `/usr/bin/journalctl --since -1h |grep "Starting Fingerprint Authentication Daemon`
